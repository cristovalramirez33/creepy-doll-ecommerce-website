// import React from 'react'
import { Container, Row, Col } from "react-bootstrap";

import products from "../assets/products";
import Product from "../components/Product";
export default function Main() {
  return (
    <Container className="py-3">
      <h3>Most Recent</h3>
      <Row>
        {products.map((product) => (
          <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
            <Product product={product} />
          </Col>
        ))}
      </Row>
    </Container>
  );
}
