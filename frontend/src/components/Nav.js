import React from "react";
import {
  Navbar,
  Nav,
  Container,
  //   NavDropdown,
  Form,
  Button,
} from "react-bootstrap";
import Logo from "../assets/img/sinisterdoll_logo.png";
function NavigationBar() {
  return (
    <>
      <Navbar bg="dark" variant="dark" expand="sm" collapseOnSelect>
        <Container fluid>
          <Navbar.Brand href="/">
            <img src={Logo} width="125px" alt="Logo" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
              <Nav.Link href="/cart">
                <i className="fas fa-shopping-cart" />
                CART
              </Nav.Link>
              <Nav.Link href="/login">
                <i className="fas fa-user" />
                LOGIN
              </Nav.Link>
            </Nav>
            <Form className="d-flex">
              <Form.Control
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
              />
              <Button variant="outline-success">Search</Button>
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default NavigationBar;
